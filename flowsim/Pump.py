from enum import Enum
from flowsim.ObservationResult import ObservationResult
from ObservationPoint import ObservationPoint, Quantity
from Position import Position
from Region import Region
import copy
import util.NumberToPflotran as ntp

def unpack(s):
    return " ".join(map(str, s))  # map(), just for kicks


class Pump:
    #note that pumps are numbered after their time of creation (e.g. the first pump's flow condition is called 'fc_pump_1' and its region is called 'region_pump_1')
    def __init__(self, id, extraction_position, injection_position, observation_position, timesteps, mass_flows, temperatures, heating, installation_cost, pump_height=30):
        self.id = id
        self.extraction_position = extraction_position
        self.injection_position = injection_position
        self.timesteps = timesteps
        self.max_mass_flow = mass_flows
        self.temperatures = temperatures
        self.heating = heating
        self.installation_cost = installation_cost
        self.state = 0
        self.pump_height = pump_height

        # also create two observations for the pumps
        self.observations = [ObservationPoint(id=f"obs_extraction_{id}",position=observation_position, quantity=Quantity.Temperature, grid=("unstructured" if pump_height == 0 else "structured"))]
        #self.observations_results = [ObservationResult(id=f"obs_extraction_{id}",position=extraction_position, quantity=Quantity.Temperature)]
        self.injection_region = f"r_injection_{self.id}"
        self.extraction_region = f"r_extraction_{self.id}"
        self.injection_fc=f"fc_injection_{self.id}"
        self.extraction_fc=f"fc_extraction_{self.id}"


    
    def get_mass_flows(self):
        if self.state == 1:
            return self.max_mass_flow
        else:
            return [0 for i in range(len(self.timesteps))]

    def tabify_list(self,lst, num_tabs=1):
        string =""
        for time_step, data in zip(self.timesteps,lst):
            for i in range(num_tabs):
                string+="\t"
            string = string + f"{ntp.ntop(time_step)} {ntp.ntop(data)}\n"
        return string

    def create_source_sink(self):
        return f"""
SOURCE_SINK
    FLOW_CONDITION {self.injection_fc}
    REGION {self.injection_region}
END

SOURCE_SINK
    FLOW_CONDITION {self.extraction_fc}
    REGION {self.extraction_region}
END
        """
    
    def create_regions(self):
        extraction_bottom = copy.deepcopy(self.extraction_position)
        injection_bottom = copy.deepcopy(self.injection_position)
        extraction_bottom.add_height(self.pump_height)
        injection_bottom.add_height(self.pump_height)
        if self.pump_height != 0:
            extraction_region = Region(f"r_extraction_{self.id}",position_start=self.extraction_position, position_end=extraction_bottom)
            injection_region =  Region(f"r_injection_{self.id}",position_start=self.injection_position, position_end=injection_bottom)
        else:
            extraction_region = Region(f"r_extraction_{self.id}",position_start=self.extraction_position)
            injection_region =  Region(f"r_injection_{self.id}",position_start=self.injection_position)
        return list(map(lambda x: x.to_pflotran(), [extraction_region,injection_region]))

    def create_observations(self):
        return list(map(lambda x: x.to_pflotran(), self.observations))

    def create_flow_condition(self):
        return f""" 
FLOW_CONDITION {self.injection_fc}
    TYPE
        RATE SCALED_MASS_RATE VOLUME
        TEMPERATURE DIRICHLET
    /

    RATE LIST
        TIME_UNITS d
        DATA_UNITS kg/s
        INTERPOLATION LINEAR
        #time   #massrate
{self.tabify_list(self.get_mass_flows(), num_tabs=1)}
    /

    TEMPERATURE LIST
        TIME_UNITS d
        DATA_UNITS C
        INTERPOLATION LINEAR
        #time   #temperature
{self.tabify_list(self.temperatures, num_tabs=1)}
    /
END

FLOW_CONDITION {self.extraction_fc}
  TYPE
    RATE SCALED_MASS_RATE VOLUME
    ENERGY_RATE ENERGY_RATE NEIGHBOR_PERM
  /
  RATE LIST 
    TIME_UNITS d 
    DATA_UNITS kg/s
    INTERPOLATION LINEAR
    #time  #massrate
{self.tabify_list([-x for x in self.get_mass_flows()], num_tabs=1)}
  /
  ENERGY_RATE 0 W
END
        """

    def to_pflotran(self):
        return f"""

{unpack(self.create_regions())}
{self.create_flow_condition()}
{self.create_source_sink()}
        """
    def printPump(self):
        print("Pump id: ",self.id,"\ncosts: ",self.installation_cost,"\nstate: ",self.state)

