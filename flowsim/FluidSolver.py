import os
from ObservationResult import ObservationResult
from ObservationPoint import ObservationPoint, Quantity
from Position import Position
import owncloud
from NextCloudUploader import NextCloudUploader
from ObservationPoint import Quantity, ObservationPoint
from ObservationResult import ObservationResult
from Position import Position
from collections import defaultdict
import re
import subprocess
import multiprocessing

from FlowModel import Model

class FluidSolver:
    def __init__(self, model, num_procs=max(1, multiprocessing.cpu_count()-2), stdout_logs=False):
        self.model = model
        self.num_procs = num_procs
        self.stdout_logs = stdout_logs


    def update_flow_model(self,model):
        self.model = model
    # takes the specified model, generates a corresponding Pflotran input file, runs the simulation
    # and returns a dictionary of observationpoints and corresponding ObservationrŔesult objects.
    def run(self, vtk):
        try:
            os.remove("pflotran.in")
        except FileNotFoundError:
            pass 
        self.write_input_file(vtk)
        print("running")

        if self.stdout_logs:
            subprocess.call(f"mpirun -n {self.num_procs} pflotran", shell=True)
        else:
            with open(os.devnull, 'w') as devnull:
                subprocess.call(f"mpirun -n {self.num_procs} pflotran", shell=True, stdout=devnull)
        observation_results = {}
        observation_maps = FluidSolver.parse_observation_all(os.getcwd())
        for d in observation_maps:
            for k, v in d.items():
                observation_results[k] = v
        items = observation_results.items()
        #for item,it3 in items:
        #    print(item + ": ")
        #    for it in it3.data:
        #        print(it)
            #print(it3.position.to_pflotran())
        #print (f"{observation_results=} results: {list(observation_results.values())[0].data=}")
        return observation_results
        
    def write_input_file(self, vtk):
        with open('pflotran.in', 'w') as pfile:
            pfile.write(self.model.to_pflotran(vtk))

    # parses all the given observationpoint data in a .tec file and returns a dictionary mapping
    # the observationpoints to a ObservationResult object
    @staticmethod
    def parse_observation_results(filename):
        with open(filename, 'r') as outputfile:
            map = {}
            counter = 1
            keys = outputfile.readline().strip().rsplit(',')
            for key in keys[1:]:
                data = list()
                values = key.strip().rsplit(' ')
                key_name= values[2]
                positionX = int(re.sub("[^0-9]", "", values[3]))
                positionY = int(re.sub("[^0-9]", "", values[4]))
                positionZ = int(re.sub("[^0-9]", "", values[5]))
                with open(filename, 'r') as readfile:
                    next(readfile)
                    for line in readfile:
                        pairs = line.strip().rsplit(' ')
                        data.append(pairs[counter * 2])
                    map[key_name] = ObservationResult( key_name, Position(positionX, positionY, positionZ), data)
                counter = counter + 1     
        return map

    #parses all .tec files in the specified folder according to parse_observation_results
    @staticmethod
    def parse_observation_all( path_to_parse):
        results = list()
        for root, dir, filenames in os.walk(path_to_parse):
            for vfile in filenames:
                if vfile.endswith('.tec'):
                    filepath = os.path.join(root, vfile)
                    results.append(FluidSolver.parse_observation_results(filepath))
                    #items = FluidSolver.parse_observation_results(filepath).data.items()
                    #for item in items:
                    #   print(item)
            break
        return results



    #Uploads all vtk files in specified path to nextcloud as zip with specified name into specified
    # nextcloudfolder. !! zip_file_name should have .zip ending"
    def upload_to_nextcloud(self, path_to_zip_from, nextcloud_folder_path, zip_file_name):
        NextCloudUploader.upload_to_nextcloud(path_to_zip_from, nextcloud_folder_path, zip_file_name)


