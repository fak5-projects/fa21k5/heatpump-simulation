"""
Provide improved python printing API for faster Coding for dyslexia

Links for speling:
1:  https://dictionary.cambridge.org/de/worterbuch/englisch/provide
2:  https://dictionary.cambridge.org/de/worterbuch/englisch/improved
3:  https://dictionary.cambridge.org/de/worterbuch/englisch/python
4:  https://dictionary.cambridge.org/de/worterbuch/englisch/printing
5:  https://dictionary.cambridge.org/de/worterbuch/englisch/api?q=API
6:  https://dictionary.cambridge.org/de/worterbuch/englisch-deutsch/for
7:  https://dictionary.cambridge.org/de/worterbuch/englisch/fast?q=faster
8:  https://dictionary.cambridge.org/de/worterbuch/englisch/code?q=coding
9:  https://dictionary.cambridge.org/de/worterbuch/englisch-deutsch/for
10: https://dictionary.cambridge.org/de/worterbuch/englisch-deutsch/dyslexia


One more recommendation in case you are that person who wants to look it up in a book: 
PONS Wörterbuch Studienausgabe Englisch
ISBN: 978-3-12-517627-0
"""
pring = print
prrin = print
prins = print
prinn = print
