from SurrogateSolver import SurrogateSolver
from Grid import Grid
from Position import Position
from TimeDomain import TimeDomain
from Pump import Pump
from FlowModel import Model
from Material import Material,FluidProperties
from BoundaryCondition import BoundaryCondition
from InitialCondition import InitialCondition
import numpy as np

bc = BoundaryCondition()
ic = InitialCondition()
time_domain = TimeDomain(360,30)
timesteps = [0,360]
grid = Grid(600,200,1,Position(600,200,30))
pumps = [
    # Pump("1",extraction_position= Position(100,100,0),injection_position=Position(100,100,0), timesteps=timesteps, mass_flows=[2,2], temperatures=[15,15] ,observation_position=Position(100,100,0),heating=True, installation_cost=1090),
    # Pump("2",extraction_position= Position(200,100,0),injection_position=Position(200,100,0), timesteps=timesteps, mass_flows=[5,5], temperatures=[5,5] ,observation_position=Position(100,200,0),heating=True, installation_cost=1090),
    Pump("3",extraction_position= Position(310,115,0),injection_position=Position(310,115,0), timesteps=timesteps, mass_flows=[2,2], temperatures=[15,15] ,observation_position=Position(100,200,0),heating=True, installation_cost=1090),
]
material = Material(1,"gravel")
fluid_props = FluidProperties()

model = Model(grid, time_domain=time_domain, materials=[material], observations=[])

solver = SurrogateSolver("./surrogate_model","./surrogate_model/domain/velocity.vtk", pumps)

eval_points = []

width = 300*2
height  = 100*2

for x in np.linspace(0,599,width):
    for y in np.linspace(0,199,height):
        for z in [0]:
            eval_points.append(np.array((x,y,z)))

eval_points = np.array(eval_points)

img = solver.run_and_image(model,eval_points).reshape((width,height))

np.savetxt("img.img",img)