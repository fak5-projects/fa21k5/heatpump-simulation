from flowsim.ObservationResult import ObservationResult
import numpy as np
from numpy.linalg import norm
from scipy.interpolate import interpn
import meshio
from tqdm.auto import tqdm
import os
from flowsim.PrintUtils import *

class SurrogateSolver:

    def __load_from_model(self, file_name):
        path = os.path.join(self.surrogate_model_folder_path, file_name+".npy")
        if not os.path.exists(path):
            raise Exception(f"{path} not found. please download model folder and velocity.vtk from nextcloud")
        return np.load(path)

    def __init__(self, flow_model, surrogate_model_folder_path, velocity_field_path, pumps, DOMAIN = (600, 200)):
        """
        Everything passed here is unchangeable
        """
        self.DOMAIN = DOMAIN
        self.surrogate_model_folder_path = surrogate_model_folder_path

        # the parameters of the surrogate model:
        # [points_amplitude,points_sigma,points_mu,points_offset]
        self.surrogate_model = self.__load_from_model("fit_parameters")

        self.pumps = pumps

        mesh = meshio.read(velocity_field_path)
        self.v_x = mesh.cell_data['Vlx'][0].reshape(DOMAIN)
        self.v_y = mesh.cell_data['Vly'][0].reshape(DOMAIN)
        self.velocity_points = [np.arange(DOMAIN[0]), np.arange(DOMAIN[1])]

        self.mass_flow_range = self.__load_from_model("mass_flow_range")
        self.temperature_range = self.__load_from_model("temperature_range")
        self.time_range = self.__load_from_model("time_range")
        self.time_distances = self.__load_from_model("time_distances")

        self.flow_model=flow_model
        self.model = flow_model

        self.mem = {}

        self.__init_streamlines()
    
    def __init_streamlines(self):
        self.streamlines_points = []
        self.streamlines_normals = []
        self.streamlines_time_distances = []
        for pump in self.pumps:
            p,n,t = self.calculate_streamline(pump.injection_position.to_numpy())
            self.streamlines_points.append(p)
            self.streamlines_normals.append(n)
            self.streamlines_time_distances.append(t)

    def __get_streamline_index_of_pump_with_id(self, id):
        for n, pump in enumerate(self.pumps):
            if pump.id == id:
                return n
        raise Exception("pump id not availdbel")

    def get_velocity(self, x, y, _):
        if (x, y) not in self.mem:
            self.mem[(x, y)] = np.array([
                interpn(self.velocity_points, self.v_x, [x, y])[0],
                interpn(self.velocity_points, self.v_y, [x, y])[0],
                0
            ])
        return self.mem[(x, y)]
    
    def calculate_streamline(self,start_point):
        forward_points = []
        forward_times = []
        forward_normals = []
        backward_points = []
        backward_times = []
        backward_normals = []
        dx = 0.5
        current = start_point
        max_steps = 1000
        n = 0
        current_time = 0
        while 0<=current[0] < self.DOMAIN[0]-1 and 0 <= current[1] < self.DOMAIN[1]-1 and n< max_steps:
            forward_points.append(current)
            vel = self.get_velocity(*current)
            forward_normals.append(np.cross(vel,np.array([0,0,1])))
            forward_times.append(current_time)
            dt = dx/norm(vel)
            current_time += dt
            current = current+vel*dt
            n+=1
        current = start_point
        n = 0
        current_time = 0
        while 0<=current[0] < self.DOMAIN[0]-1 and 0 <= current[1] < self.DOMAIN[1]-1 and n< max_steps:
            backward_points.append(current)
            vel = self.get_velocity(*current)
            backward_normals.append(np.cross(vel,np.array([0,0,1])))
            backward_times.append(current_time)
            dt = -dx/norm(vel)
            current_time += dt
            current = current+vel*dt
            n+= 1
        ns = np.array(backward_normals[1:][::-1] + forward_normals)
        magnitudes = norm(ns,axis = 1)
        ns[...,0]/= magnitudes
        ns[...,1]/= magnitudes
        return np.array(backward_points[1:][::-1] + forward_points), ns, np.array(backward_times[1:][::-1] + forward_times)


    def run_and_image(self, flow_model, eval_points):
        """
        Does what it does
        """
        # TODO fix crash when no pump is active

        def gauss(x, amplitude, sigma, mu, offset):
            """
            Gauss
            """
            return amplitude/sigma * np.exp(-np.square(x-mu)/(2*sigma*sigma))+offset
        n_active_pumps = 0
        total_temperatures = np.zeros(eval_points.shape[0])
        for n,pump in enumerate(self.flow_model.pumps):
            if pump.state != 0:
                n_active_pumps += 1
                streamline_index = self.__get_streamline_index_of_pump_with_id(
                        pump.id)
                for observation_index, observation_coordinate in tqdm(enumerate(eval_points),total = len(eval_points),desc=f"{n+1}/{len(self.pumps)}"):
                    nearest_streamliné_point_index = np.argmin(
                        norm(observation_coordinate - self.streamlines_points[streamline_index], axis=1))
                    constants = (pump.max_mass_flow[0], pump.temperatures[0],
                                 flow_model.time_domain.days/30, self.streamlines_time_distances[streamline_index][ nearest_streamliné_point_index])
                    if constants[3] > self.time_distances[-1] or constants[3] < self.time_distances[0]:
                        total_temperatures[observation_index] += 10
                        continue
                    normal = self.streamlines_normals[streamline_index][nearest_streamliné_point_index]
                    distance = normal @ (observation_coordinate -
                                         self.streamlines_points[streamline_index][nearest_streamliné_point_index])/self.DOMAIN[1]*2
                    all_points_for_the_interpolation_please = [
                        self.mass_flow_range, self.temperature_range, self.time_range, self.time_distances]

                    points_amplitude, points_sigma, points_mu, points_offset = self.surrogate_model

                    amplitude = interpn(all_points_for_the_interpolation_please,
                                        points_amplitude, constants)
                    sigma = interpn(all_points_for_the_interpolation_please,
                                    points_sigma, constants)
                    mu = interpn(all_points_for_the_interpolation_please,
                                 points_mu, constants)
                    offset = interpn(all_points_for_the_interpolation_please,
                                     points_offset, constants)
                    total_temperatures[observation_index] += gauss(distance,
                                                                   amplitude, sigma, mu, offset)
        return total_temperatures/n_active_pumps


    def __generate_observation_result(self,id,observation,temperature):
        return ObservationResult(id,observation.pos,[temperature])

    def run(self,vtk):
        """
        Does what it does
        """
        if self.flow_model is None: 
            raise Exception("Please call SurrogateSolver.update_flow_model(newModel) before calling run, to give it a flow model (pump configuration)")
        def gauss(x, amplitude, sigma, mu, offset):
            """
            Gauss
            """
            return amplitude/sigma * np.exp(-np.square(x-mu)/(2*sigma*sigma))+offset
        n_active_pumps = 0
        eval_points =np.array([observation_point.pos.to_numpy() for observation_point in  self.flow_model.observations])
        max_temperatures = np.zeros(eval_points.shape[0])
        #prinn(max_temperatures)
        for n,pump in enumerate(self.model.pumps):
            if pump.state != 0:
                n_active_pumps += 1
                streamline_index = self.__get_streamline_index_of_pump_with_id(
                        pump.id)
                for observation_index, observation_coordinate in enumerate(eval_points):
                    nearest_streamliné_point_index = np.argmin(
                        norm(observation_coordinate - self.streamlines_points[streamline_index], axis=1))
                    constants = (pump.get_mass_flows()[0], pump.temperatures[0],
                                 self.flow_model.time_domain.days/30, self.streamlines_time_distances[streamline_index][ nearest_streamliné_point_index])
                    if constants[3] > self.time_distances[-1] or constants[3] < self.time_distances[0]:
                        max_temperatures[observation_index] = max(10,max_temperatures[observation_index])
                        continue
                    normal = self.streamlines_normals[streamline_index][nearest_streamliné_point_index]
                    distance = normal @ (observation_coordinate -
                                         self.streamlines_points[streamline_index][nearest_streamliné_point_index])/self.DOMAIN[1]*2
                    all_points_for_the_interpolation_please = [
                        self.mass_flow_range, self.temperature_range, self.time_range, self.time_distances]

                    points_amplitude, points_sigma, points_mu, points_offset = self.surrogate_model

                    amplitude = interpn(all_points_for_the_interpolation_please,
                                        points_amplitude, constants)
                    sigma = interpn(all_points_for_the_interpolation_please,
                                    points_sigma, constants)
                    mu = interpn(all_points_for_the_interpolation_please,
                                 points_mu, constants)
                    offset = interpn(all_points_for_the_interpolation_please,
                                     points_offset, constants)
                    temperature = gauss(distance,amplitude, sigma, mu, offset)
                    
                    #pring(f"T: {temperature} at: {observation_coordinate}")
                    if temperature > max_temperatures[observation_index]:
                        max_temperatures[observation_index] = temperature
        if n_active_pumps == 0:
            mean_temperatures = np.full_like(max_temperatures,10) 
        else:
            mean_temperatures =  max_temperatures
        #print(f"DADA{mean_temperatures}")
        results = {self.flow_model.observations[index].id: self.__generate_observation_result(self.flow_model.observations[index].id, self.flow_model.observations[index],temperature) for index, temperature in enumerate(
        mean_temperatures)}
        return results
