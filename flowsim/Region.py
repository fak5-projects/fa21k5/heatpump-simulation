class Region:
    def __init__(self, name, position_start, position_end=None, face=""):
        self.name = name
        self.positionStart = position_start
        self.positionEnd = position_end
        self.face = face
        
    def to_pflotran(self):
        if self.positionEnd is not None:
            return f"""
    REGION {self.name}
        COORDINATES
            {self.positionStart.to_pflotran()}
            {self.positionEnd.to_pflotran()}
        /
        {"FACE "+self.face if self.face != "" else ""}
    END
            """
        else:
            return f"""
    REGION {self.name}
        COORDINATE {self.positionStart.to_pflotran()}
    END
            """