import util.NumberToPflotran as ntp
class Grid:
    #give number of cells in x, y, z-direction and Position (xmax,ymax,zmax) defining the grid box size together with coordinate (0,0,0)
    def __init__(self, cellnumber_x, cellnumber_y, cellnumber_z, bound_pos):
        self.cellX = cellnumber_x
        self.cellY = cellnumber_y
        self.cellZ = cellnumber_z
        self.bound_pos = bound_pos


    def create_bounds(self):
        return f"""
REGION west
    COORDINATES
        0.000000d0 0.000000d0 0.000000d0
        0.000000d0 {ntp.ntop(self.bound_pos.y)} {ntp.ntop(self.bound_pos.z)}
    /
    FACE WEST
END

REGION east
    COORDINATES
       {ntp.ntop(self.bound_pos.x)} 0.000000d0 0.000000d0
       {ntp.ntop(self.bound_pos.x)} {ntp.ntop(self.bound_pos.y)} {ntp.ntop(self.bound_pos.z)}
    /
    FACE EAST
END

REGION ALL
    COORDINATES
       0.0d0 0.000000d0 0.000000d0
       {ntp.ntop(self.bound_pos.x)} {ntp.ntop(self.bound_pos.y)} {ntp.ntop(self.bound_pos.z)}
    /
END


        
        """
    def to_pflotran(self):
        return f"""
        {self.create_bounds()}
        GRID
            TYPE STRUCTURED
            NXYZ {str(self.cellX)} {str(self.cellY)} {str(self.cellZ)}
            BOUNDS
                0.d0 0.d0 0.d0
                {self.bound_pos.to_pflotran()}
            /
        END
        """