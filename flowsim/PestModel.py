from Grid import Grid
from TimeDomain import TimeDomain
from Pump import Pump
from ObservationPoint import ObservationPoint
from InitialCondition import InitialCondition
from BoundaryCondition import BoundaryCondition
from optimization.TopologyClass import Topology


class PestModel:
    #expects (Grid, TimeDomain, List of Pumps, List of ObservationPoints).
    #if you don't want to have any pumps, just give an empty list, e.g. 'list()'
    #to give a permeability field, put an .h5-file named permeability_data.h5 into the folder of all the python files
    def __init__(self,model_folder, topology, time_domain, materials, observations, initial_conditions=[InitialCondition()], boundary_conditions=BoundaryCondition.default_BCs()):
        self.grid = topology.grid
        self.time_domain = time_domain
        self.pumps = topology.list_of_pumps
        self.materials = materials
        self.observations = observations
        self.initial_conditions = initial_conditions
        self.boundary_conditions = boundary_conditions
        self.model_folder = model_folder


        self.observations = ([c for s in map(lambda x: x.observations, self.pumps) for c in s])

    def get_observation_quantities(self):
        quantities = {}
        for observation in self.observations:
            quantities[observation.quantity] = observation.quantity
        return quantities.keys()

    def get_simulation_block(self):
        return """
SIMULATION
    SIMULATION_TYPE SUBSURFACE
    PROCESS_MODELS
        SUBSURFACE_FLOW flow
        MODE TH
        /
    /
END
        """
    

    def get_numerical_methods_block(self):
        return """
NUMERICAL_METHODS FLOW 
    NEWTON_SOLVER 
        ANALYTICAL_JACOBIAN 
        ITOL_UPDATE 1.d0 
        RTOL 1.d-20 
    / 
    LINEAR_SOLVER 
        SOLVER ITERATIVE 
    / 
END
        """
    
    def get_output_file_vtk(self):
        return f"""
        OUTPUT
            SNAPSHOT_FILE
                {self.time_domain.snapshot_contrib()}
                FORMAT HDF5
                PRINT_COLUMN_IDS
                VARIABLES
                    {"".join([q.to_pflotran() for q in self.get_observation_quantities()])}
                /
                VELOCITY_AT_CENTER
            /   
            OBSERVATION_FILE
                {self.time_domain.observation_contrib()}
                VARIABLES
                    {"".join([q.to_pflotran() for q in self.get_observation_quantities()])}
                /
            /
        END
        """

    def get_output_file_novtk(self):
        return f"""
                OUTPUT
                    OBSERVATION_FILE
                        {self.time_domain.observation_contrib()}
                        VARIABLES
                            {"".join([q.to_pflotran() for q in self.get_observation_quantities()])}
                        /

                    /
                END
                """


    def to_pflotran(self, vtk):
        return f"""
        {self.get_simulation_block()}
        SUBSURFACE
        REFERENCE_PRESSURE 101325.
        GRID
            # for explicit
            #TYPE UNSTRUCTURED_EXPLICIT ./test_model/test_ascii_implicit.uge
            #UPWIND_FRACTION_METHOD CELL_VOLUME

            # for implicit
            TYPE UNSTRUCTURED {self.model_folder}/test_ascii_implicit.ugi
            MAX_CELLS_SHARING_A_VERTEX 50
        END
        {self.get_numerical_methods_block()}
        FLUID_PROPERTY
            DIFFUSION_COEFFICIENT 1.d-9
        /
        DATASET perm
            HDF5_DATASET_NAME Permeability
            FILENAME {self.model_folder}/permeability.h5
        END
        MATERIAL_PROPERTY gravel
            ID 1
            POROSITY 0.25d0
            TORTUOSITY 0.5d0
            ROCK_DENSITY 2.8E3
            SPECIFIC_HEAT 1E3
            THERMAL_CONDUCTIVITY_DRY 0.5
            THERMAL_CONDUCTIVITY_WET 0.5
            LONGITUDINAL_DISPERSIVITY 3.1536d0
            PERMEABILITY
                VERTICAL_ANISOTROPY_RATIO 0.1
                DATASET perm
            /
            CHARACTERISTIC_CURVES cc1
        /
        CHARACTERISTIC_CURVES cc1
            SATURATION_FUNCTION VAN_GENUCHTEN
                ALPHA  1.d-4
                M 0.5d0
                LIQUID_RESIDUAL_SATURATION 0.1d0
            /
            PERMEABILITY_FUNCTION MUALEM_VG_LIQ
                M 0.5d0
                LIQUID_RESIDUAL_SATURATION 0.1d0
            /
        END

        REGION all
        FILE {self.model_folder}/test_implicit_materials_and_regions.h5
        END

        REGION south
        FILE {self.model_folder}/SouthBC.ss
        END

        REGION north
        FILE {self.model_folder}/NorthBC.ss
        END

        {self.time_domain.to_pflotran()}

        FLOW_CONDITION initial
            TYPE
                PRESSURE HYDROSTATIC
                TEMPERATURE DIRICHLET
            /
            DATUM 0.d0 0.d0 10.d0
            GRADIENT
                PRESSURE -0.002 0. 0.
            /
            PRESSURE 101325.d0
            TEMPERATURE 10.d0
        END

        FLOW_CONDITION inflow_series
            TYPE
                PRESSURE HYDROSTATIC
                TEMPERATURE DIRICHLET
            /
            DATUM FILE {self.model_folder}/bc_inflow.txt
            PRESSURE 101325.d0
            TEMPERATURE 10.d0
        END

        FLOW_CONDITION outflow_series
            TYPE
                PRESSURE HYDROSTATIC
                TEMPERATURE DIRICHLET
            /
            DATUM FILE {self.model_folder}/bc_outflow.txt
            PRESSURE 101325.d0
            TEMPERATURE 10.d0
        END

        INITIAL_CONDITION all
            FLOW_CONDITION initial
            REGION all
        END


        BOUNDARY_CONDITION inflow
            FLOW_CONDITION inflow_series
            REGION south
        END

        BOUNDARY_CONDITION outflow
            FLOW_CONDITION outflow_series
            REGION north
        END

        {self.get_output_file_vtk()}

        {"".join([obs.to_pflotran() for obs in self.observations])}
        STRATA
            REGION all
            MATERIAL gravel
        END
        {"".join([pump.to_pflotran() for pump in self.pumps])}
        

        HDF5_READ_GROUP_SIZE 1
        END_SUBSURFACE
                
        """
