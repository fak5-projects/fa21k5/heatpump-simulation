from ObjectiveFunctionClass import ObjectiveFunction

def Factory(objective_function_parameters):
    non_pump_energy = objective_function_parameters.get('non_pump_energy_cost')
    return CostObjectiveFunction(non_pump_energy)

class CostObjectiveFunction(ObjectiveFunction):

    def __init__(self, non_pump_energy):
        self.non_pump_energy = non_pump_energy

    def evaluate(self, topology):
        cost = 0.0
        for pump in topology.list_of_pumps:

            cost = cost + float(pump.state)*pump.installation_cost
        cost = cost + float(topology.getPumps() - topology.getActivePumps())*self.non_pump_energy 
        return cost