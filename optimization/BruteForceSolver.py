""" This is a Brute Force Solver that tries all the possible pump combinations and gives the best."""

import numpy as np
from itertools import product
import optimization.SolverClass as Optimization

def Factory(solver_parameters):
    convergence_tolerance = solver_parameters.get('convergence_tolerance')
    return BruteForceSolver(convergence_tolerance)

class BruteForceSolver(Optimization.Solver):
    def BuildProblem(self, fluid_solver,objective_function, list_of_constraints, topology, time_domain, materials, observations, initial_metric_value=10, metric='msqe'):
        super().BuildProblem(fluid_solver,objective_function, list_of_constraints, topology, time_domain, materials, observations, initial_metric_value=initial_metric_value, metric=metric)
        self.best_topology = topology
        self.best_topology_objective_function_value = objective_function.evaluate(topology)
        self.fluid_solver = fluid_solver
        # self.best_topology_objective_function_value = 1e10

    def RunLoop(self):

        possible_topologies = list(product([True,False], repeat = self.topology.getPumps()))
        #i_topology = [True, True, False, False]
        list_of_costs = []
        for i_topology in possible_topologies:
            print(i_topology)
            self.topology.updatePumps(i_topology)
            self.UpdateModel()
            self.DeleteTecFiles()
            results = self.Evaluate()
            list_of_costs.append(results)
            if not isinstance(results, bool):
                if results < self.best_topology_objective_function_value:
                    self.best_topology = self.topology
                    self.best_i_topology = i_topology
                    self.best_topology_objective_function_value = results
        print(possible_topologies)
        print(list_of_costs)


    def Evaluate(self):

        #self.fluid_solver.model = self.topology.GenerateAsFlowModel()
        observation_results = self.fluid_solver.run(True)

        if not all(list(map(lambda x: x.check(self.topology, observation_results), self.list_of_constraints))):
            return False

        return self.objective_function.evaluate(self.topology)


        

            


