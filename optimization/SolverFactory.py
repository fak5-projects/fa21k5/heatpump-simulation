from importlib import import_module

class SolverFactory(object):

    def __init__(self):
        pass

    def CreateSolver(self,solver_parameters):

        solver_type_name = solver_parameters.get('solver_type')
        solver_type_class = import_module(solver_type_name)
        specific_solver_parameters = solver_parameters.get('solver_parameters')

        return solver_type_class.Factory(specific_solver_parameters)