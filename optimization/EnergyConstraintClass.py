from ConstraintClass import Constraint
 
def Factory(constraint_parameters):
    energy_demand = constraint_parameters.get('energy_demand')
    return EnergyConstraint(energy_demand)

class EnergyConstraint(Constraint):
    def __init__(self, energy_demand):
        self.energy_demand = energy_demand

    def check(self, topology):
        pass