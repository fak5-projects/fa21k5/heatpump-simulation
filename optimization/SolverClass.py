""" This is the base Solver class for the Optimization process."""
import numpy as np
from flowsim.FluidSolver import *
import os
class Solver:

    def __init__(self,convergence_tolerance, max_iterations = 20) -> None:

        self.convergence_tolerance = convergence_tolerance
        self.convergence_criteria = lambda difference: difference < convergence_tolerance
        self.max_iterations = max_iterations

    def BuildProblem(self,fluid_solver, objective_function, list_of_constraints, topology, time_domain, materials, observations, initial_metric_value = 10, metric = 'msqe'):
        """This method builds the initial problem for the solver."""

        self.objective_function = objective_function
        self.list_of_constraints = list_of_constraints
        self.topology = topology
        self.time_domain = time_domain
        self.materials = materials
        self.observations = observations
        self.metric_value = initial_metric_value
        self.metric = metric
        self.fluid_solver = fluid_solver

        self.UpdateModel()

    def RunLoop(self):
        """This method runs the optimization loop. It can be modified as needed."""

        results_previous = None
        iteration = 0

        if iteration < self.max_iterations:
            while self.CheckConvergence(self.metric_value) == False:
                results = self.Evaluate()
                if results_previous is None:
                    results_previous = np.zeros_like(results)
                self.metric_value = self.CalculateMetric(results, results_previous)
                results_previous = results
            iteration = iteration+1
            

    def Evaluate(self):
        """This method evaluate the objective function and the constraints."""

        print("WARNING: Evaluate should be defined in the child solver.")


    def CheckConvergence(self):
        """This method checks the convergence of the solver with respect to a metric."""
        print("Metric value = %.10f" % self.metric_value)
        print("Convergence tolerance %.10f" % self.convergence_tolerance)
        print(self.convergence_criteria(self.metric_value))
        return self.convergence_criteria(self.metric_value)

    def CalculateMetric(self, results, results_previous):
        """This method calculates a metric value from the results between two timesteps."""

        if self.metric == 'msqe':
            print("Results: %.10f" %results)
            print("Results previous %.10f"%results_previous)
            value = np.sqrt(np.power(results-results_previous,2))
            print("Value:%.10f"%value)
            return value


    def UpdateModel(self):
        # this method is kinda pointless since the pumps are referenced anyway.
        self.topology.PrintConfiguration()
        #self.flow_model = Model(self.topology, self.time_domain, self.materials, self.observations )
        #self.fluid_solver = FluidSolver(self.flow_model)

    #all .tec-files produced at the end of a PFLOTRAN simulation must be removed before starting a new simulation because the parsing of these files (see FluidSolver.py) won't work otherwise
    def DeleteTecFiles(self):
        for filename in os.listdir(os.getcwd()):
            if filename.endswith(".tec"):
                os.remove(filename)

