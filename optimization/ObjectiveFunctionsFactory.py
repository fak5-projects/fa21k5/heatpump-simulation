from importlib import import_module

class ObjectiveFunctionsFactory(object):

    def __init__(self):
        pass

    def CreateObjectiveFunction(self,objective_function_parameters):

        objective_function_type_name = objective_function_parameters.get('objective_type')
        objective_function_type_class = import_module(objective_function_type_name)
        specific_objective_function_parameters = objective_function_parameters.get('objective_parameters')

        return objective_function_type_class.Factory(specific_objective_function_parameters)