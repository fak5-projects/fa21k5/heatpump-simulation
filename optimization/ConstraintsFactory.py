from importlib import import_module

class ConstraintsFactory(object):

    def __init__(self):
        pass

    def CreateConstraints(self,constraint_parameters):

        constraint_type_name =  constraint_parameters.get('constraint_type')
        constraint_type_class = import_module(constraint_type_name)
        specific_constraint_parameters = constraint_parameters.get('constraint_parameters')

        return constraint_type_class.Factory(specific_constraint_parameters)