""" This is a Genetic Algorithm implementation for the optimization process.
Author: Optimization Team, Course 05, Ferienakademie 2021
Reference: Tutorial on Genetic Algorithm, Prof. Duddeck, Structural Optimization 2, TUM.
"""

import numpy as np
# np.random.seed(42) # Random seed for testing purposes
import distutils.util
from itertools import product
import optimization.SolverClass as Optimization
import os

LIBRARY_CACHE_PATH = 'library_cache.npy'

def Factory(solver_parameters):
    convergence_tolerance = solver_parameters.get('convergence_tolerance')
    try:
        max_iterations = solver_parameters.get('max_iterations')
    except AttributeError:
        max_iterations = 20
    initial_population_size = solver_parameters.get('initial_population')
    crossover_probability = solver_parameters.get('crossover_probability')
    keep_criteria = solver_parameters.get('keep_criteria')
    mutation_probability = solver_parameters.get('mutation_probability')
    penalty = solver_parameters.get('penalty')
    library = solver_parameters.get('library')

    return GeneticAlgorithmSolver(convergence_tolerance, max_iterations, initial_population_size, crossover_probability,
                                  keep_criteria, mutation_probability, penalty, library  )

class GeneticAlgorithmSolver(Optimization.Solver):

    def __init__(self, convergence_tolerance, max_iterations=20, initial_population_size = 5,
                crossover_probability = 0.7, keep_criteria = 'random', mutation_probability = 0.05, penalty = 10000.0, library = False ) -> None:

        super().__init__(convergence_tolerance, max_iterations=max_iterations)
        self.initial_population_size = initial_population_size
        self.crossover_probability = crossover_probability
        self.keep_criteria = keep_criteria
        self.mutation_probability = mutation_probability
        self.penalty = penalty
        self.library_status = library

    def BuildProblem(self, fluid_solver,objective_function, list_of_constraints, topology, time_domain, materials, observations, initial_metric_value=10, metric='msqe'):
        super().BuildProblem(fluid_solver,objective_function, list_of_constraints, topology, time_domain, materials, observations, initial_metric_value=initial_metric_value, metric=metric)
        self.best_topology = topology
        self.best_topology_objective_function_value = objective_function.evaluate(topology)
        self.fluid_solver = fluid_solver
        if self.library_status:
            if os.path.exists(LIBRARY_CACHE_PATH):
                self.library = np.load(LIBRARY_CACHE_PATH,allow_pickle='TRUE').item()
            else:
                self.library = dict()

    def RunLoop(self):

        # Initialize the population
        G = self.Initial_Population(self.initial_population_size)
        initial_G  = G
        i=0
        convergence_flag = False
        previous_max_fraction = 0.0
        iteration = 0
        list_of_W = []
        list_of_G = [G]

        while (not convergence_flag) and (iteration < self.max_iterations):

            print("Iteration: %d" %iteration)
            # prepare new generation
            G_previous = G
            W = self.Roulette_Wheel(G)
            print("Wheel: ", W)
            M1 = self.Mating(G, W)
            print("M1    :", M1)
            M2 = self.Mating(G, W)
            print("M2    :", M2)
            C = self.Crossover(G, M1, M2, probability=self.crossover_probability, keep=self.keep_criteria)  # default: 0.7
            print("Cross: ",C)
            G = self.Mutation(C, probability=self.mutation_probability) # default: 0.05
            print("Mutat: ", G)
            print("~init: ", initial_G)
            single_fraction = 0.0
            for i in range(len(W)):
                if i == 0:
                    new_max_fraction = W[i]
                    index = 0
                else:
                    new_max_fraction = W[i]-W[i-1]
                if new_max_fraction > single_fraction:
                    single_fraction = new_max_fraction
                    index = i 
                #print("Current index: %d" %index)
                #print("Current fraction: %.5f" %single_fraction)
                #print(G_previous[index])
            self.best_i_topology = G_previous[index]
            self.metric_value = self.CalculateMetric(single_fraction, previous_max_fraction)
            convergence_flag = self.CheckConvergence()
            previous_max_fraction = single_fraction
            list_of_W.append(W)
            list_of_G.append(G)

            iteration = iteration + 1
        
        for wheel in list_of_W:
            print("Fractions for iteration %d" % list_of_W.index(wheel))
            print(wheel)
            print("Individuals in porpulation %d" % list_of_W.index(wheel))
            print(list_of_G[list_of_W.index(wheel)])

        #save library cache
        #TODO save only if new values were added
        np.save(LIBRARY_CACHE_PATH, self.library) 

    def Evaluate(self):

        #self.fluid_solver.model = self.topology.GenerateAsFlowModel()
        observation_results = self.fluid_solver.run(False)

        if not all(list(map(lambda x: x.check(self.topology, observation_results), self.list_of_constraints))):
            return self.penalty

        return self.objective_function.evaluate(self.topology)

    def Fitness(self, individual):

        if self.library_status:
            fitness = self.CheckAndUpdateLibrary(individual)
        else:
            individual_as_bools = distutils.util.strtobool(list(individual))
            self.topology.updatePumps(individual_as_bools)
            self.UpdateModel()
            self.DeleteTecFiles()
            fitness = self.Evaluate()

        return fitness

    def CheckAndUpdateLibrary(self, individual):
        try:
            fitness = self.library[individual]
        except KeyError:
            print(individual)
            individual_as_bools = list(map(distutils.util.strtobool,list(individual)))
            print(individual_as_bools)
            self.topology.updatePumps(individual_as_bools)
            self.UpdateModel()
            fitness = self.Evaluate()
            self.library[individual] = fitness
        return fitness

    def Initial_Population(self, n):
        """
        Creates the initial population with 'n' individuals
        """
        sl = self.topology.getPumps() # Length of the binary string of the individuals
        Generation = []
        for i in range(n):
            Individual = ''
            for j in range(sl):
                Individual += str(np.random.choice([0, 1]))      
                # if j == 0 or j == 3:
                #     Individual += '1'
                # else:
                #     Individual += '0'        
                    
            Generation.append(Individual)
        return Generation

    def Roulette_Wheel(self,G):
        """
        Returns a roulette wheel for a given population 'G' according to the
        fitness value of the individuals.
        """
        fracs = []
        wheel = []
        
        fG_minimization = [self.Fitness(i) for i in G]
        fG = [max(fG_minimization) - x for x in fG_minimization]
        if all(abs(v) < 1e-10 for v in fG):
            fG = fG_minimization
        print(fG)
        for i in range(len(G)):
            fracs.append(float(fG[i]) / float(sum(fG)))
        for i in range(len(fracs)):
            if i == 0:
                wheel.append(fracs[i])
            else:
                wheel.append(wheel[i - 1] + fracs[i])
        return wheel

    def Mating(self, G, W):
        """
        Returns a mating pool. For each individual in the population 'G', the
        roulette wheel 'W' is turned once from which the id of a mate is selected
        """
        Mates = []
        for Ind in G:
            test = np.random.rand()
            for i in range(len(W)):
                if i == 0:
                    if test <= W[i] and test > 0.0:
                        mate = i
                else:
                    if test <= W[i] and test > W[i - 1]:
                        mate = i
            Mates.append(mate)
        return Mates

    def Crossover(self,G, M1, M2, probability=1.0, keep=1):
        """
        Performs one-point crossover for a population 'G', two parents are selected
        using two mating pools 'M1' and 'M2'. Crossover is performed according to
        the 'probability' variable, if probability = 1.0, crossover is performed
        for every individual in the population, if probability = 0.0, crossover
        is not performed, i.e. the closer the value of the probability is to zero,
        crossover is less likely to happen, the closer to 1.0, crossover is most
        likely to occur. Only one offspring is kept to form the new generation,
        the 'keep' variable controls this decision, keep can be 1, 2 or 'random',
        1 for keeping the first offspring, 2 the second, and 'random' to decide
        with a coin toss.
        """
        New_Generation = []
        
        assert keep in ["random", 1, 2]

        for i in range(len(G)):
            # figure out where to crossover
            if np.random.rand() < probability:
                cp = np.random.randint(1, len(G[i]) - 1)  # cp .. cross point
            else:
                cp = 0
            Child_1 = G[M1[i]][:cp] + '|' + G[M2[i]][cp:]
            Child_2 = G[M2[i]][:cp] + '|' + G[M1[i]][cp:]
            Children = [Child_1, Child_2]

            # remove the divider
            if keep == 'random':
                keep = np.random.choice([1, 2])

            # decide which child to keep (no pun intended)
            kept = Children[keep - 1].replace("|", "")

            New_Generation.append(kept)
        return New_Generation

    def Mutation(self, G, probability=0.001, print_all=False):
        """
        Performs mutation for every individual in the population 'G' according to
        the 'probability' variable.
        """
        New_Generation = []
        
        for i, Ind in enumerate(G):
            New_Ind = ''
            for ch in Ind:
                if np.random.rand() < probability:
                    if ch == '1':
                        new_ch = '0'
                    if ch == '0':
                        new_ch = '1'
                    New_Ind += new_ch
                else:
                    New_Ind += ch
            New_Generation.append(New_Ind)
        return New_Generation
