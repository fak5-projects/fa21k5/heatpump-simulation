from ConstraintClass import Constraint

def Factory(constraint_parameters):
    max_temperature = constraint_parameters.get('t_max')
    min_temperature = constraint_parameters.get('t_min')
    return TemperatureConstraint(max_temperature, min_temperature)

class TemperatureConstraint(Constraint):
    def __init__(self, max_temperature, min_temparature):
        self.max_temperature = max_temperature
        self.min_temperature = min_temparature

    def check(self, topology, observation_results):
        for pump in topology.list_of_pumps:
                print(pump.id)
                print(float(observation_results[pump.observations[0].id].data[-1]))
                if pump.state:
                    if pump.heating:
                        if float(observation_results[pump.observations[0].id].data[-1]) < self.min_temperature:
                            return False
                    else:
                        if float(observation_results[pump.observations[0].id].data[-1]) > self.max_temperature:
                            return False
        return True

