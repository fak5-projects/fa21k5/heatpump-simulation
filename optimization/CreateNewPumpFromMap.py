from flowsim.Position import Position
from flowsim.Pump import Pump

def CreateNewPumpFromMap(pump_parameters):

    coord_ex = Position(pump_parameters.get('x_coord_ex'),pump_parameters.get('y_coord_ex'),0)
    coord_in = Position(pump_parameters.get('x_coord_in'),pump_parameters.get('y_coord_in'),0)
    observation_position = coord_ex
    observation_position.z = pump_parameters.get('z_coord')
    pump_id = pump_parameters.get('pump_id')
    cost = pump_parameters.get('cost')
    massflow = pump_parameters.get('massflow')
    pump_height = 30 if pump_parameters.get('pump_height') == None else pump_parameters.get('pump_height')  # nice hardcoded value here :)
    heating = int(pump_parameters.get('heating'))
    base_temperature = pump_parameters.get('base_temperature')
   
    pump = Pump(pump_id, coord_ex, coord_in, observation_position, [0], [massflow], [base_temperature], heating, cost, pump_height=pump_height)

    return pump   