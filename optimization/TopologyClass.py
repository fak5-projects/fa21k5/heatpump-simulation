import numpy as np
from flowsim.Pump import Pump
class Topology:

    def __init__(self, list_of_pumps, grid):
        self.list_of_pumps = list_of_pumps
        self.grid = grid

    def getPumps(self):
        return len(self.list_of_pumps)
    
    def getActivePumps(self):
        active_pumps = 0 
        for pumps in self.list_of_pumps:
            active_pumps = active_pumps + int(pumps.state)
        return active_pumps

    def updatePumps(self, pumps_state):
        for (state, pump) in zip(pumps_state, self.list_of_pumps):
            pump.state = state

    def PrintConfiguration(self):
        configuration = []
        for pump in self.list_of_pumps:
            configuration.append(pump.state)
        print (configuration)

    def OrderPumpsBy(self, order):
        if order == "cost":
            self.list_of_pumps.sort(key=self.GetCostOfPump) 


    def GetCostOfPump(self, pump):
        return pump.installation_cost
