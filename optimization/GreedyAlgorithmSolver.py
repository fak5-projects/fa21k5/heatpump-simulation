""" This is a Brute Force Solver that tries all the possible pump combinations and gives the best."""

import numpy as np
from itertools import product
import optimization.SolverClass as Optimization

def Factory(solver_parameters):
    convergence_tolerance = solver_parameters.get('convergence_tolerance')
    order = solver_parameters.get('order')
    return GreedyAlgorithmSolver(convergence_tolerance, order)
class GreedyAlgorithmSolver(Optimization.Solver):

    def __init__(self, convergence_tolerance, max_iterations=20, order='cost') -> None:
        super().__init__(convergence_tolerance, max_iterations=max_iterations)
        self.order = order

    
    def BuildProblem(self,fluid_solver, objective_function, list_of_constraints, topology, time_domain, materials, observations, initial_metric_value=10, metric='msqe'):
        super().BuildProblem(fluid_solver,objective_function, list_of_constraints, topology, time_domain, materials, observations, initial_metric_value=initial_metric_value, metric=metric)
        self.best_topology = topology
        self.best_topology_objective_function_value = objective_function.evaluate(topology)
        self.topology.OrderPumpsBy(self.order)
        self.fluid_solver = fluid_solver

    def RunLoop(self):

        list_of_pumps = np.zeros(self.topology.getPumps()).astype(bool)
        for i in range(len(list_of_pumps)):

            list_of_pumps[i] = True
            self.topology.updatePumps(list_of_pumps)
            self.UpdateModel()
            self.DeleteTecFiles()
            results = self.Evaluate()
            if isinstance(results, bool):
                list_of_pumps[i] = False
                self.topology.updatePumps(list_of_pumps)
        self.best_topology = self.topology
        self.best_i_topology = list_of_pumps
        self.best_topology_objective_function_value = results
        print(self.best_i_topology)
        print([p.printPump() for p in self.topology.list_of_pumps])


    def Evaluate(self):

        observation_results = self.fluid_solver.run(False)

        if not all(list(map(lambda x: x.check(self.topology, observation_results), self.list_of_constraints))):
            return False

        return self.objective_function.evaluate(self.topology)


        

            


