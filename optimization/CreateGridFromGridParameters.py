from flowsim.Position import Position
from flowsim.Grid import Grid

def CreateGridFromGridParameters(grid_parameters):

    x_cells = grid_parameters.get('grid_x_cells')
    y_cells = grid_parameters.get('grid_y_cells')
    z_cells = grid_parameters.get('grid_z_cells')
    grid_x_dimension = grid_parameters.get('grid_x_dimension')
    grid_y_dimension = grid_parameters.get('grid_y_dimension')
    grid_z_dimension = grid_parameters.get('grid_z_dimension')
    grid = Grid(x_cells, y_cells, z_cells, Position(grid_x_dimension, grid_y_dimension, grid_z_dimension))

    return grid