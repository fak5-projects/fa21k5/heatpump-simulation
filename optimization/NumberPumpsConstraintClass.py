from ConstraintClass import Constraint

def Factory(constraint_parameters):
    max_number_pumps = constraint_parameters.get('lower_bound')
    min_number_pumps = constraint_parameters.get('upper_bound')
    return NumberPumpsConstraint(max_number_pumps, min_number_pumps)


class NumberPumpsConstraint(Constraint):
    def __init__(self, max_number_pumps, min_number_pumps):
        self.max_pumps = max_number_pumps
        self.min_pumps = min_number_pumps

    def check(self, topology, observation_results):
        return True
        #return topology.getActivePumps() >= self.min_pumps and self.topology.getActivePumps() <= self.max_pumps  
