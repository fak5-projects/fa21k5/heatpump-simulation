from optimization.CreateNewPumpFromMap import CreateNewPumpFromMap

def GenerateGridOfPumps(number_of_pumps_x, number_of_pumps_y, coordinates_first_pump, coordinates_last_pump, separation_in_ex, pump_parameters):
    """This function generates a list of pumps in a rectangular grid."""

    number_of_pumps = number_of_pumps_x * number_of_pumps_y
    list_of_pumps = []
    x_range = coordinates_last_pump[0] - coordinates_first_pump[0]
    y_range = coordinates_last_pump[1] - coordinates_first_pump[1]
    x_separation = x_range/(number_of_pumps_x-1)
    y_separation = y_range/(number_of_pumps_y-1)

    for pump_number in range(number_of_pumps):
        new_parameters = pump_parameters
        new_parameters['pump_id'] = pump_number
        new_parameters['x_coord_ex'] = coordinates_first_pump[0]+x_separation*(number_of_pumps % number_of_pumps_y)
        new_parameters['y_coord_ex'] = coordinates_first_pump[1]+y_separation*(number_of_pumps % number_of_pumps_x)
        new_parameters['x_coord_in'] = new_parameters['x_coord_ex'] + separation_in_ex
        new_parameters['y_coord_in'] = new_parameters['y_coord_ex']
        list_of_pumps.append(CreateNewPumpFromMap(new_parameters))

    return list_of_pumps
