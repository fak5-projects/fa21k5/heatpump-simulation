from main import *
import matplotlib.pyplot as plt
import sys


number_of_runs = 5000
list_of_results = []
running_time = []

file_path = 'caseFourPumpsGenetic.json'

with open(file_path) as file:
    case_parameters = json.load(file)

    for i in range(number_of_runs):

        results = main(case_parameters)
        list_of_results.append(results[0])
        running_time.append(results[1])

    print(list_of_results)
    print(running_time)

    dict_results= {i:list_of_results.count(i) for i in list_of_results}
    print(dict_results, file=sys.stderr) # workaround for > /dev/null
    plot = plt.figure()
    plt.bar(dict_results.keys(), dict_results.values())

    plt.savefig('barplot.png')
