# PEST

## Prerequisites

Download pflotran.h5 and test_model from nextcloud.fa.

Links:

1. [test_model](https://nextcloud.fa/apps/files/?dir=/CompleteOptimisationWorkflow/PFLOTRAN/test_model&fileid=7945)
2. [pflotran.h5](https://nextcloud.fa/remote.php/webdav/CompleteOptimisationWorkflow/PFLOTRAN/PermeabilityTest/permeability_REV5/pflotran.h5)

Place these in [pest/PFLOTRAN](pest/PFLOTRAN)

## How to run the simulation

Run `simulation_run.sh` or alternatively go to the pest/PEST folder and run `pestpp-glm liquid_head_case.pst` from there. You can supply an optional argument to the bash script
which specifies the pst-file. Be careful other test cases (like test_pst.pst etc.) might need other pfoltran.h5 files which are _somewhere_ on nextcloud. By default pflotran is run
with a degree of parallelization of 3, changing this might result in faster runtimes. For this change the number of the `-n` argument to `mpiexec`
in [pest/bin/model_command_FA.bat](pest/bin/model_command_FA.bat).

The output permeability field is written to pest/PFLOTRAN/permeability.h5

## Developer information

simulation_run.sh calls `pestpp-glm `with liquid_head_case.pst

### liquid_head_case.pst

- loads pilot pilot point positions (20)
- loads observation points (16)
- loads observation values (16)
- switches to command line and calls model_command_FA.bat

- #### model_command_FA.bat
    - calls interpolation.py

    - ##### interpolation.py
        - reads pilot point positions(x,y) and values(z) from permpp.dat
        - compute rbf(x,y)
        - reads mesh from pflotran_boxmodel.h5 and set up xi,yi
        - evaluate rbf on every (xi,yi) and write it to a matrix: \[x y value\]
        - write this to permeability.h5

    - remove old pflotran output "*.tec" file
    - runs pflotran with pflotran.* files using
    - calls pflotranoutput_to_ssf_FA_parallel.py: converts pflotran output to ssf
    - calls tsproc

- switches to input/output mode

- writes pilot points to permpp.dat

- switches to regularization mode  
             

