#!/bin/bash
#
# pilot points preprocessing
python3 ../bin/interpolation.py
#
rm ../PFLOTRAN/*.tec # remove old output
# Running wait in the foreground does not work when using pest so wait runs in background
# and immediately waits
# 3 cores is fastest on my machine. Try what works for you.
mpiexec -n 3  pflotran -input_prefix ../PFLOTRAN/pflotran > log.pflotran &
wait
python3 ../bin/pflotranoutput_to_ssf_FA_parallel.py # converter
cd ../PEST
tsproc < ../PEST/tsproc.in # runs tsproc