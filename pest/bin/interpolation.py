import os
import scipy.interpolate as interpolate
import numpy as np
import pandas as pd
import csv
import h5py
from io import StringIO

# define all file names of input and output
PFLOTRAN_DIR = '../PFLOTRAN/'
PFLOTRAN_BOXMODEL_FILE = PFLOTRAN_DIR + 'pflotran_boxmodel.h5'
PERMEABILITY_FILE = PFLOTRAN_DIR + 'permeability.h5'
DATA_DIR = '../Data/'
DATA_PERMPP_DAT_FILE = DATA_DIR + 'permpp.dat'

DEBUG = False

def debug_print(s, *args):
    if DEBUG:
        print(f"{s} {args}")

def interpolation_main():
    # read pilot points from .dat file
    pilot_points = read_pilot_points()
    debug_print('\nPilot_points: \n', pilot_points)

    # save data from pilot points in extra lists for the interpolation
    x_coord_pp = pilot_points.loc[:, "x_coord"]
    y_coord_pp = pilot_points.loc[:, "y_coord"]
    permeability_pp = pilot_points.loc[:, "permeability"]

    # get grid information
    x_grid, y_grid = read_grid()

    # checks whether all points are inside the grid
    if check_extrapolation(x_grid, y_grid, x_coord_pp, y_coord_pp):
        print("[ABORTING INTERPOLATION] Please check your input data, some data points appear to be outside the field")
        exit(1)

    # call the interpolation Rbf
    rbf_function = interpolate.Rbf(x_coord_pp, y_coord_pp, np.log(permeability_pp), function='thin-plate')
    interpolated_permeability = np.exp(rbf_function(x_grid, y_grid))

    debug_print("min value: " + str(np.min(interpolated_permeability)) + ", max value: " + str(np.max(interpolated_permeability)))
    debug_print('interpolated permeability: \n', interpolated_permeability)

    # save grid as np array
    cells_grid = np.array([x_grid, y_grid])
    debug_print('\ncells_grid: \n', cells_grid)
    # writes the permeability file from the guessed permeability on the grid cells
    write_permeability_file(interpolated_permeability, cells_grid)

    # Print the created h5 file
    interpolated_points = read_output_file(PERMEABILITY_FILE)
    debug_print(interpolated_points)
    print("[INTERPOLATION] Successful")

def check_extrapolation(x_domain, y_domain, x_points, y_points):
    """
    Checks whether all provided data points are inside the domain :param x_domain: x-coordinates of the domain to
    test against :param y_domain: y-coordinates of the domain to test against :param x_points: x-coordinates of the
    test points :param y_points: y-coordinates of the test points :return: whether it's an interpolation or
    extrapolation, True means it's an extrapolation and the simulation should be aborted
    """
    is_extrapolation = False
    max_discrepancy = 0.15

    # rearrange field data, point data
    domain = np.array([x_domain, y_domain]).T
    points = np.array([x_points, y_points]).T

    # define area surrounding the domain to include points slightly outside the domain
    domain_x_diff = np.max(x_domain) - np.min(x_domain)
    domain_y_diff = np.max(y_domain) - np.min(y_domain)

    surroundings_x_max = np.max(x_domain) + max_discrepancy * domain_x_diff
    surroundings_x_min = np.min(x_domain) - max_discrepancy * domain_x_diff
    surroundings_y_max = np.max(y_domain) + max_discrepancy * domain_y_diff
    surroundings_y_min = np.min(y_domain) - max_discrepancy * domain_y_diff

    # for each point evaluate whether it is in the direct surroundings of the domain, if not: return false
    for point in points:
        if point[0] > surroundings_x_max or point[0] < surroundings_x_min or point[1] > surroundings_y_max or point[1] < surroundings_y_min:
            is_extrapolation = True
            return is_extrapolation

    return is_extrapolation


def read_pilot_points():
    # read pilot points from .dat file

    if not (os.path.isfile(DATA_PERMPP_DAT_FILE)):
        print("It appears you do not have at least some of the data files. Please download them from Nextcloud.")
        quit(1)

    with open(DATA_PERMPP_DAT_FILE) as dat_file, StringIO("") as csv_buffer:
        csv_writer = csv.writer(csv_buffer)

        for line in dat_file:
            row = [field.strip() for field in line.split()]
            csv_writer.writerow(row)
        csv_buffer.seek(0)

        data = pd.read_csv(csv_buffer, names=["ID", "x_coord", "y_coord", "n", "permeability"])
    return data


def write_permeability_file(perm_array, cells_grid):
    # writes the permeability input file for PEST
    number_cells = perm_array.shape[0]
    ids = np.arange(1, number_cells + 1)

    assert number_cells == cells_grid.shape[1]

    file = h5py.File(PERMEABILITY_FILE, "w")
    # NOTE there may be PFLOTRAN problems as the ids are integers now
    cell_ids = file.create_dataset("Cell Ids", data=ids)
    perm_vals = file.create_dataset("Permeability", data=perm_array)

    debug_print('Wrote to permeability_values.h5: Cell Ids: \n', cell_ids[:], "\n")
    debug_print('Wrote to permeability_values.h5: Permeability: \n', perm_vals[:], "\n")


def read_grid():
    # reads grid file in .h5 format
    with h5py.File(PFLOTRAN_BOXMODEL_FILE, "r") as file:
        debug_print('\nGroup keys in input h5 file: \n', list(file.keys())[0])

        dataset = file['Domain']

        list_data_XC = [key for key in dataset['XC']]
        list_data_YC = [key for key in dataset['YC']]
    return list_data_XC, list_data_YC


def read_output_file(filename):
    with h5py.File(filename, "r") as file:

        debug_print('\nGroup keys in created h5 file: \n', list(file.keys()))

        # dataset = file['Permeability']

        list_data_XC = [key for key in file['Cell Ids']]
        list_data_YC = [key for key in file['Permeability']]
    return list_data_XC, list_data_YC


if __name__ == '__main__':
    interpolation_main()

