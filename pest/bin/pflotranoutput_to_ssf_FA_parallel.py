# Script to gather PFLOTRAN observation outputs and convert the tecplot format to TSPROC ssf format

import glob
import os
import re

import numpy as np
import datetime

#################
# Set a start date
#################
print("Start SSF para")
start_date = "11/10/1991 12:01:26"

#################
# Set the timestep interval for time series export
#################
timeStep = 0.001  # unit in days

# get PFLOTRAN observation files
filepath = '../PFLOTRAN'
array = []

for root, dir_, filenames in os.walk(filepath):
    for vfile in filenames:
        if vfile.endswith('.tec'):
            filename = os.path.join(root, vfile)
            with open(filename, 'r') as f:
                lines = f.readlines()
                keys = lines[0].strip().split(",")
                words = lines[2].strip().split("  ")
                if len(keys) != len(words):
                    print("Fail")
                    quit(1)
                for j in range(1, len(keys)):
                    key = keys[j]
                    word = words[j]
                    key_name = re.search(r"obs_\d+", key.strip())
                    if key_name is None:
                        print("No obs key found")
                        quit(10)

                    index = int(re.sub("obs_", "", key_name.group(0)))
                    array.append((key_name.group(0) + ' ' + start_date + ' ' + word, index))

with open('../PFLOTRAN/pflotran_output.ssf', "w") as file_out:
    array.sort(key=lambda x: x[1])
    file_out.write('\n'.join(map(lambda x: x[0], array)))

# --------Writing SSF file --------------

# File must be written in form

# obs_1 11/10/1991 12:01:26 12.00(=permeability)
# obs_2 11/10/1991 12:01:26 11.83
# obs_3 11/10/1991 12:01:26 12.81
# '''

## Output file with name "pflotran_output.ssf" in PFLOTRAN location

print("End SSF para")
