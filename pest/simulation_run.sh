#!/bin/bash
cd "$(dirname "$0")"/PEST || exit
if [ ! -d ../PFLOTRAN/test_model  ]; then
  echo "test_model missing. Please get it from nextcloud."
  exit 10
fi
if [ ! -f ../PFLOTRAN/pflotran.h5 ]; then
  echo "pflotran.h5 missing. Please get it from nextcloud."
  exit 10
fi
pestpp-glm "${1:-liquid_head_case.pst}"


