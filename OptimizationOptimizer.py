from main import *
import numpy as np
import sys

max_iterations = 40
number_of_runs = 15

running_time = []
known_best = '1001'


file_path = 'caseFourPumpsGenetic.json'

def step_float(old_value, gradient, minimum, maximum, step_scale):
    return min(maximum, max(minimum, old_value + gradient * step_scale))

with open(file_path) as file:
    case_parameters = json.load(file)
    crossover_scale = case_parameters['solver_parameters']['solver_parameters']['crossover_probability']
    mutation_scale = case_parameters['solver_parameters']['solver_parameters']['mutation_probability']
    initial_population = case_parameters['solver_parameters']['solver_parameters']['initial_population']
    crossover = 1.0
    mutation = 1.0
    minimum_crossover = 0.5
    maximum_crossover = 1.0/crossover_scale
    minimum_mutation = 0.0
    maximum_mutation = 3.0
    initial_population_step = 1
    initial_population_maximum = 16
    initial_population_minimum = 2
    crossover_step_scale = 0.005
    mutation_step_scale = 0.08

    for opt_iterations in range(max_iterations):
        dict_results = dict()
        list_of_results = []
        case_parameters['solver_parameters']['solver_parameters']['initial_population'] = initial_population
        case_parameters['solver_parameters']['solver_parameters']['crossover_probability'] = crossover * crossover_scale
        case_parameters['solver_parameters']['solver_parameters']['mutation_probability'] = mutation * mutation_scale
        

        for i in range(number_of_runs):

            results = main(case_parameters)
            list_of_results.append(results[0])

        dict_results= {i:list_of_results.count(i) for i in list_of_results}
        #print(dict_results, file=sys.stderr) # workaround for > /dev/null
        
        known_best_proportion = dict_results[known_best]/number_of_runs
        print(f"pop: {initial_population}, x: {crossover * crossover_scale}, mut: {mutation * mutation_scale}, best_prop: {known_best_proportion}", file=sys.stderr) # workaround for > /dev/null


        if opt_iterations == 0:
            previous_initial_population = 0
            previous_crossover = 0.0
            previous_mutation = 0.0
            previous_best_proportion = 0.0
        
        difference = known_best_proportion - previous_best_proportion
        try:
            gradient_initial_population = difference / (initial_population - previous_initial_population)
        except ZeroDivisionError:
            gradient_initial_population = 0
        try:
            gradient_crossover = difference / (crossover - previous_crossover)
        except ZeroDivisionError:
            gradient_crossover = 0.0
        try:
            gradient_mutation = difference / (mutation - previous_mutation)
        except ZeroDivisionError:
            gradient_mutation = 0.0

        previous_initial_population = initial_population
        previous_crossover = crossover
        previous_mutation = mutation
        previous_best_proportion = known_best_proportion

        crossover = step_float(crossover, gradient_crossover, minimum_crossover, maximum_crossover, crossover_step_scale)
        mutation = step_float(mutation, gradient_mutation, minimum_mutation, maximum_mutation, mutation_step_scale)
        initial_population += np.sign(gradient_initial_population) * initial_population_step
        initial_population = int(min(initial_population_maximum, max(initial_population_minimum, initial_population)))

    print(f"pop: {initial_population}, x: {crossover * crossover_scale}, mut: {mutation * mutation_scale}", file=sys.stderr) # workaround for > /dev/null

