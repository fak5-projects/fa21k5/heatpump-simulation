""" This is the main script for the optimizer.
From a given case scenario, the optimal location of the pumps is given.

Input: case scenario,  parameters, solver
Output: results from optimal pump distribution

Authors: Ferienakademie 2021,  Team"""

import sys
import os
sys.path.append('./flowsim')
sys.path.append('./optimization')
from optimization.GenerateGridOfPumps import GenerateGridOfPumps
from optimization.ObjectiveFunctionsFactory import ObjectiveFunctionsFactory
from optimization.ConstraintsFactory import ConstraintsFactory
from optimization.SolverFactory import SolverFactory
from flowsim.FluidSolver import FluidSolver
from flowsim.Position import Position
from flowsim.Grid import Grid
from flowsim.Pump import Pump
from flowsim.Material import Material, FluidProperties
from flowsim.TimeDomain import TimeDomain
from flowsim.FlowModel import Model
from optimization.DummySolver import DummyFluidSolver
from optimization.TopologyClass import Topology
from optimization.CreateNewPumpFromMap import CreateNewPumpFromMap
from optimization.CreateGridFromGridParameters import CreateGridFromGridParameters
from optimization.TemperatureConstraintClass import TemperatureConstraint
from flowsim.PestModel import PestModel

import json
import time

# def Initialize(file_path):

def RunOptimizationLoop(solver):
    """Runs the  loop with a given solver."""

    solver_vtk_flag = False # Run without output

    solver.RunLoop()
     

def Finalize(solver):
    """Performs all task at the end of the  (e.g. updates the topology, runs a simulation for visualization."""

    print(solver.best_i_topology)
    

if __name__ == "__main__":

    t = time.time()

    file_path = 'caseFourPumpsPest.json'

    with open(file_path) as file:
        case_parameters = json.load(file)

    # Pump initialization (hard coded)
    if "coordinates_last_pump" in case_parameters:
        list_of_pumps = GenerateGridOfPumps(case_parameters.get("number_of_pumps_x"),case_parameters.get("number_of_pumps_y"),case_parameters("coordinates_first_pump"),case_parameters("coordinates_last_pump"),case_parameters("separation_in_ex"),case_parameters("pump_parameters"))
    else:
        list_of_pumps_parameters = case_parameters.get('list_of_pumps')
        list_of_pumps = []

        for pump_parameters in list_of_pumps_parameters:
            list_of_pumps.append(CreateNewPumpFromMap(pump_parameters))

    grid = CreateGridFromGridParameters(case_parameters.get('grid_parameters'))
    # add pumps to topology

    topology = Topology(list_of_pumps, grid) 

    print(topology.list_of_pumps)
    #we are assuming observation points at pumps only

    # define constraints (as class)
    list_of_constraints_parameters = case_parameters.get('list_of_constraints')
    constraints_factory = ConstraintsFactory()
    list_of_constraints = []
    for constraint_parameters in list_of_constraints_parameters:
        list_of_constraints.append(constraints_factory.CreateConstraints(constraint_parameters))

    # define OF (as class)
    list_of_objective_parameters = case_parameters.get('list_of_objective_functions')
    objective_factory = ObjectiveFunctionsFactory()
    list_of_objective = []
    for objective_parameters in list_of_objective_parameters:
        list_of_objective.append(objective_factory.CreateObjectiveFunction(objective_parameters))

    # initialize solver ( brute force -> OF and constraints, topology)
    solver_parameters = case_parameters.get('solver_parameters')
    solver_factory = SolverFactory()
    solver = solver_factory.CreateSolver(solver_parameters)

    material = Material(1, "gravel")
    fluid_props = FluidProperties()
    materials = [material, fluid_props]
    observations = []

    time_domain_parameters = case_parameters.get('time_domain_parameters')
    time_domain = TimeDomain(time_domain_parameters.get('days'), time_domain_parameters.get('maximal_time_step'))

    model_path = os.path.join(os.getcwd(), "pest_model")
    flow_model = PestModel(model_path,topology, time_domain, materials, observations)
    
    
    fluid_solver = FluidSolver(flow_model, 1)
    solver.BuildProblem(list_of_objective[0], list_of_constraints, topology, time_domain, materials, observations)
    # create while  loop. run flow with no vtk output
    RunOptimizationLoop(solver)
    #fluid_solver.write_input_file(True)
    # run again best configuration with vtk output

    Finalize(solver)

    elapsed = time.time() - t
    print('The program took %d time units.' % elapsed)



